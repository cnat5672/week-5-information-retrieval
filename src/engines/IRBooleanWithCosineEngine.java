package engines;

import core.DTMatrix;
import core.Vector;

import java.util.HashMap;
import java.util.Map;

/**
 * Boolean Retrieval Engine
 */
public class IRBooleanWithCosineEngine extends Engine {

    private Vector idf;
    private static final double CUT_OFF = 0.05;

    /**
     * Initialise the IR Boolean Engine
     * Hint: You may wish to calculate the IDF vector and apply it to the matrix here. Look at dtMatrix.multiply(...)
     */
    public IRBooleanWithCosineEngine(DTMatrix dtMatrix) {
        // this.dtMatrix = dtMatrix;
    }

    /**
     * Returns the results of executing a query using the argument text
     * 1. Create the query vector
     * 2. Convert the query vector to it's tf-idf counterpart
     * 3. Calculate the similarity score between the query vector and each document in the matrix
     * 4. Normalise this score
     * 4. If the score is greater than CUT_OFF, add it to the results Map
     * @param text - the query to execute
     * @return a map of (document) => (score)
     */
    @Override
    public Map<String, Double> execute(String text) {
        Map<String, Double> results = new HashMap<String, Double>();

        return results;
    }

    /**
     * Calculates the normalisation value from a vector
     * Formula = sqrt(\sum{vector[i]^2}), or see lecture slides
     */
    protected double calculateNormalisationScalar(Vector vector) {
        return 0;
    }
}
