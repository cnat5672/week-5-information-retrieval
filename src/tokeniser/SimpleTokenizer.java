package tokeniser;

import core.Constants;

import java.util.ArrayList;

/**
 * Simple tokeniser to convert a text string into an array of tokens.
 * - Remove all punctuation and numbers
 * - Return only the words that remain (delimited by whitespace)
 * - Ignore stop words
 */
public class SimpleTokenizer implements Tokenizer {
    @Override
    public String[] parse(String text) {
        ArrayList<String> words = new ArrayList<String>();
        /** Token the text **/
        return words.toArray(new String[words.size()]);
    }
}
